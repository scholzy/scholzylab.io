---
title: Does the triphenylamine-based D35 dye sensitizer form aggregates on metal-oxide surfaces?
layout: default
modal-id: 2
date: 2016-02-23
img: d35_sensitiser.png
thumbnail: d35_sensitiser-thumbnail.png
alt: image-alt
project-date: April 2014
description: Lorem ipsum dolor sit amet, usu cu alterum nominavi lobortis. At duo novum diceret. Tantas apeirian vix et, usu sanctus postulant inciderint ut, populo diceret necessitatibus in vim. Cu eum dicam feugiat noluisse.

---
